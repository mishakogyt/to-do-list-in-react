import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import './app.css';
import SidePanel from '../sidePanel'


function App() {
  return (
    <Router>
      <div className='wrapper'>
      <SidePanel/>
      </div>
    </Router>
  );
}

export default App;
