import React, {useState} from 'react';
import axios from 'axios'
import {useHistory, Route, Link} from "react-router-dom";

import './loginPage.css';

function LoginPage({changeStatus, ucFirst}) {
   const [login, setLogin] = useState();
   const [password, setPassword] = useState();
   const [name, setName] = useState("name");
   const [email, setEmail] = useState("");
   const [loginReg, setLoginReg] = useState("");
   const [passwordReg, setPasswordReg] = useState("");
   const [authorized, setAuthorized] = useState(false);
   
   let history = useHistory();


   const entry = () => {
      axios.get('http://localhost:3001/users').then(({ data }) => {
         data.map(data => {
            if(data.login === login && data.password === password){
               axios.patch('http://localhost:3001/users/' + data.id, {
                  "authorized": true
                  
               })
               setAuthorized(true);
               changeStatus(true);

            }
            return data;
         })
         // if(!authorized){
         //    alert('Неверный логин или пароль');
         // }
      })
      .catch(() => {
         alert('Ошибка');
      })
   
   }

   const checkIn = () => {
      const obj =  {
         "login": loginReg,
         "name": ucFirst(name),
         "password": passwordReg,
         "email": email,
         "authorized": false
      };

      axios.post('http://localhost:3001/users', obj).then(({data}) => {
         
         alert('Поздравляем Вы успешно зарегистрировались!');
         history.push(`/`);
         
         axios.post(`http://localhost:3001/lists`, { name: "Первый список",colorId: 1, userId: data.id})
         .then(({data}) => {
            const objTask = [
               {
                  "listId": data.id,
                  "text": "Редактируй эту запись",
                  "completed": false
               },
               {
                  "listId": data.id,
                  "text": "Удали эту запись",
                  "completed": false
               },
               {
                  "listId": data.id,
                  "text": "Отметь выполненной эту",
                  "completed": false
               },
               {
                  "listId": data.id,
                  "text": "Редактируй названия списка",
                  "completed": false
               },
               {
                  "listId": data.id,
                  "text": "Зайди в личный кабинет, нажав на имя",
                  "completed": false
               },
               {
                  "listId": data.id,
                  "text": "Добавь новую папку",
                  "completed": false
               },
               {
                  "listId": data.id,
                  "text": "Добавь новый таск",
                  "completed": false
               }
            ];
         });
      })
      .catch(e => {
      alert('Ошибка');
      })
      axios.get('http://localhost:3001/users').then(({data}) => {    
         data.map(user => {
            if(user.login !== loginReg) {
                  
            }
         })     
      });
   }

   return (
      <div className="login-page">
         <Route exact path={authorized ? null : "/"}>
            <h2 className="login__title">TO DO LIST</h2>
            <p className="login__pretitle">вход</p>
            <input
               onChange={(e) => {                  
                  setLogin(e.target.value);
               }} 
               type="text" 
               placeholder="Логин">   
            </input>
            <input 
               onChange={(e) => {                  
                  setPassword(e.target.value);
               }} 
               type="text" 
               placeholder="Пароль">   
            </input>
            <button className="log active" onClick={entry}>Войти</button>
            <Link to="/registration"><button className="reg">Регистрация</button></Link>
         </Route>
         
         <Route path="/registration">
            <h2 className="login__title">TO DO LIST</h2>
            <p className="login__pretitle">регистрация</p>
            <input
               onChange={(e) => {                  
                  setName(e.target.value);
               }} 
               type="text" 
               placeholder="Имя">   
            </input>
            <input
               onChange={(e) => {                  
                  setEmail(e.target.value);
               }} 
               type="email" 
               placeholder="Email">   
            </input>
            <input
               onChange={(e) => {                  
                  setLoginReg(e.target.value);
               }} 
               type="text" 
               placeholder="Логин">   
            </input>
            <input 
               onChange={(e) => {                  
                  setPasswordReg(e.target.value);
               }} 
               type="text" 
               placeholder="Пароль">   
            </input>
            <button className="reg active" onClick={checkIn}>Регистрация</button>
            <Link to="/"><button className="log">Войти</button></Link>
         </Route>
      </div>
   )
}

export default LoginPage;