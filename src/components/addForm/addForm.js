import React, {useState} from 'react';
import axios from 'axios'

import './addForm.css';

function AddForm({list, onAddTask, ucFirst}) {
   const [showForm, setShowForm] = useState(false);
   const [inputValue, setInputValue] = useState('');
   let formClass = "add-form__form";

   if(showForm){
      formClass += " show"
   }

   const addTask = () => {
      const obj =  {
         "listId": list.id,
         "text": inputValue,
         "completed": false,
         "important": false,
         "id": Math.random()
      };

      axios.post('http://localhost:3001/tasks', obj).then(({ data }) => {
      onAddTask(list.id, obj);
        setShowForm(false);
      })
      .catch(e => {
        alert('Ошибка при добавлении задачи!');
      })

   }

   return (
      <div className="add-form" >
         <button 

            onClick={() => setShowForm(true)}
            className={showForm ? "add-form__button hide" : "add-form__button"}>
            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M8 1V15" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
            <path d="M1 8H15" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
            </svg>
            Новая задача
         </button>

         <form className={`${formClass}`}>
            <input 
               placeholder="Текст задачи"
               value={inputValue}
               onChange={(e) => {                  
                  setInputValue(ucFirst(e.target.value));
               }} 
               type="text">
            </input>
            <div className="add-form__buttons">
               <button 

                  type="submit" 
                  onClick={(e) => {
                     e.preventDefault();
                     setInputValue('');
                     addTask();
                  }} 
                  className="add-form__button-add">
                  Добавить задачу
               </button>
               <button 
                  onClick={(e) => {
                     e.preventDefault()
                     setShowForm(false);
                     setInputValue('');
                  }} 
                  className="add-form__button-cancel">
                  Отмена
               </button>
            </div>
         </form>
      </div>
   )
}

export default AddForm;