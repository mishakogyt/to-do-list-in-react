import React, {useEffect, useState} from 'react';
import axios from 'axios'
import {useHistory, Route, Link} from "react-router-dom";

import './sidePanel.css';
import FolderItem from '../folderItem';
import LoginPage from '../loginPage';
import Workspace from '../workspace';
import PrivateOffice from '../privateOffice';

import all from './img/all.svg';
import close from './img/close.svg';

const SidePanel = () => {
   const [selected, changeSelected] = useState(false);
   const [lists, setLists] = useState([]);
   const [colors, setColor] = useState(null);
   const [activeItem, setActiveItem] = useState({});
   const [privateOffice, setPrivateOffice] = useState(false);
   const [selectedColor, selectColor] = useState(1);
   const [inputValue, setInputValue] = useState(""); 
   const [status, setStatus] = useState(false);
   const [user, setUser] = useState();
   let history = useHistory();
   let modalClass = "folder-modal";

   if(selected){
      modalClass += " show"
   }

   const getUser = () => {
      axios.get('http://localhost:3001/users').then(({ data }) => {
         data.map(data => {
            if(data.authorized){
               setStatus(data.authorized);
               setUser(data);
            }
         return data;
         });
      });
   }

   const updateList = () => {
      axios.get('http://localhost:3001/lists?_expand=color&_embed=tasks').then(({ data }) => {
         if(user){
            const lists = data.filter(l => l.userId === user.id);
            setLists(lists);
         }
      });
   }

   useEffect (() => {
      axios.get('http://localhost:3001/colors').then(({ data }) => {
         setColor(data);        
      });
      getUser();
   }, [status, history.location.pathname])

   useEffect(() => {
      if(Array.isArray(colors)){
         selectColor(colors[0].id);
      }
      updateList();
   }, [colors, user])

   useEffect(() => {
      const listId = history.location.pathname.split('lists/')[1];
      if (lists.length > 1) {
        const list = lists.find(list => list.id === Number(listId));
        setActiveItem(list);
      }      
   }, [lists, history.location.pathname])

   const removeActive = () => {
      setActiveItem(null)
   }

   const onEditUser = () => {
      getUser();
   }

   const onClose = () => {
      changeSelected(false);
      setInputValue('');
      selectColor(colors[0].id);
   }

   const ucFirst = (str) => {
      if (!str) return str;
      return str[0].toUpperCase() + str.slice(1);
   }

   const addList = (e) => {
      if(!inputValue) {
         alert('Введите название папки!');
         return;
      }
      const title = ucFirst(inputValue)
      setInputValue(title);
      console.log(ucFirst(inputValue))
      

      axios.post(`http://localhost:3001/lists`, { name: inputValue,colorId: selectedColor, userId: user.id})
         .then(({data}) => {
         const color = colors.filter(c => c.id === selectedColor)[0];
         const obj = { ...data, color: color, tasks: []}
         onAddList(obj);
         onClose();
      });
   }

   const onAddList = (obj) => {
      const newList = [ ...lists, obj]
      setLists(newList);     
   }
   
   const onEditListTitle = (id, title) => {
      const newList = lists.map(item => {
         if(item.id === id){
            item.name = title;
         }
         return item;
      });
      setLists(newList); 
   }

   const onAddTask = (listId, taskObj) => {
      const newList = lists.map(item => {
         if(item.id === listId){
            item.tasks = [...item.tasks, taskObj];
         }
         return item;
      });
      setLists(newList);    
   }
   
   const onEditTask = (taskId, listId, newTaskText) => {
      if(newTaskText){
         const newList = lists.map(list => {
            if(list.id === listId){
               list.tasks = list.tasks.map(task => {
                  if (task.id === taskId) {
                  task.text = newTaskText;
                  }
                  return task;
               });
            }
            return list;
         });
         setLists(newList);
      }
   }

   const onRemoveTask = (listId, taskId) => {
      if (window.confirm('Вы действительно хотите удалить задачу?')) {
      const newList = lists.map(item => {
         if (item.id === listId) {
            item.tasks = item.tasks.filter(task => task.id !== taskId);

         }
         return item;
      });
      setLists(newList);

      axios.delete('http://localhost:3001/tasks/' + taskId).catch(() => {
         alert('Не удалось удалить задачу');
      });
    }
   }

   const onCompleteTask = (listId, taskId, list) => {
      const newList = lists.map(list => {
        if (list.id === listId) {
          list.tasks = list.tasks.map(task => {
            if (task.id === taskId) {
              task.completed = !task.completed;
            }
            return task;
          });
        }
        return list;
      });
      setLists(newList);

      const task = list.tasks.filter(task => task.id === taskId)[0].completed;
      
      axios
        .patch('http://localhost:3001/tasks/' + taskId, {
            "completed": task
        })
        .catch(() => {
            alert('Не удалось обновить задачу');
        });
   };

   const onImportantTask = (listId, taskId, list) => {
      console.log(listId, taskId, list)
      const newList = lists.map(list => {
        if (list.id === listId) {
          list.tasks = list.tasks.map(task => {
            if (task.id === taskId) {
              task.important = !task.important;
            }
            return task;
          });
        }
        return list;
      });
      setLists(newList);

      const task = list.tasks.filter(task => task.id === taskId)[0].important;
      
      axios
        .patch('http://localhost:3001/tasks/' + taskId, {
            "important": task
        })
        .catch(() => {
            alert('Не удалось обновить задачу');
        });
   };

   const changeStatus = (authorized) => {
      setStatus(authorized);  
   }


   const exit = () => {
      axios.patch('http://localhost:3001/users/' + user.id, {
         "authorized": false
      }).then(() => {
         setUser(null);
         setStatus(false);
         history.push(`/`);
      })
   }

   

   return (
      <>
         {status ? null : <LoginPage ucFirst={ucFirst} onAddTask={onAddTask} changeStatus={changeStatus} onAddList={onAddList}/>}
         {status ? (
            <>
            <div className="side-panel">
               <div className="user side-panel__button-all flex">
               <Link to="/privateOffice" onClick={() => {
                  setPrivateOffice(true);
                  setActiveItem(false);
                  }}>
                  <p>{user ? user.name : null}</p>
               </Link>
               <svg onClick={exit} height="15" viewBox="0 0 512.00533 512" width="15" xmlns="http://www.w3.org/2000/svg">
                  <path d="m320 277.335938c-11.796875 0-21.332031 9.558593-21.332031 21.332031v85.335937c0 11.753906-9.558594 21.332032-21.335938 21.332032h-64v-320c0-18.21875-11.605469-34.496094-29.054687-40.554688l-6.316406-2.113281h99.371093c11.777344 0 21.335938 9.578125 21.335938 21.335937v64c0 11.773438 9.535156 21.332032 21.332031 21.332032s21.332031-9.558594 21.332031-21.332032v-64c0-35.285156-28.714843-63.99999975-64-63.99999975h-229.332031c-.8125 0-1.492188.36328175-2.28125.46874975-1.027344-.085937-2.007812-.46874975-3.050781-.46874975-23.53125 0-42.667969 19.13281275-42.667969 42.66406275v384c0 18.21875 11.605469 34.496093 29.054688 40.554687l128.386718 42.796875c4.351563 1.34375 8.679688 1.984375 13.226563 1.984375 23.53125 0 42.664062-19.136718 42.664062-42.667968v-21.332032h64c35.285157 0 64-28.714844 64-64v-85.335937c0-11.773438-9.535156-21.332031-21.332031-21.332031zm0 0"/>
                  <path d="m505.75 198.253906-85.335938-85.332031c-6.097656-6.101563-15.273437-7.9375-23.25-4.632813-7.957031 3.308594-13.164062 11.09375-13.164062 19.714844v64h-85.332031c-11.777344 0-21.335938 9.554688-21.335938 21.332032 0 11.777343 9.558594 21.332031 21.335938 21.332031h85.332031v64c0 8.621093 5.207031 16.40625 13.164062 19.714843 7.976563 3.304688 17.152344 1.46875 23.25-4.628906l85.335938-85.335937c8.339844-8.339844 8.339844-21.824219 0-30.164063zm0 0"/>
               </svg>

               </div>
               <Link to="/">
               <div 
                  className={privateOffice || activeItem ? "side-panel__button-all" : "side-panel__button-all active" }
                  onClick={() => {
                     setActiveItem(false)
                     setPrivateOffice(false)
                  }}>
                     <button>
                        <img src={all} alt="all"></img>
                        Все задачи
                     </button>
               </div> 
               </Link>

               <FolderItem 
                  items={lists} 
                  color={colors}
                  onRemove={id => {
                  const newList = lists.filter(item => item.id !== id);
                  setLists(newList);     
                  history.push(`/`);
                  }}
                  
                  onClickItem={item => {
                     setActiveItem(item);
                     setPrivateOffice(false)
                     history.push(`/lists/${item.id}`);
                  }}
                  activeItem={activeItem}
               />


               <div className="side-panel__button-all-folder ">
                     <button
                        onClick={() => changeSelected(true)}> 
                        <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                           <path d="M6 1V11" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
                           <path d="M1 6H11" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
                        </svg>
                        Добавить папку
                     </button>
               </div> 

               <div className={modalClass}>
                  <img onClick={onClose} className="close" src={close} alt="close"></img>
                  <input 
                     key="editor1"
                     placeholder="Название папки"
                     value={inputValue}
                     onChange={(e) => {                  
                        setInputValue(ucFirst(e.target.value))
                     }} 
                     type="text">
                  </input>
                  
                  <div className="folder-cyrcle__color-block flex">
                     {/* Рендерим цвета */}
                     {colors ? colors.map((color) => (
                        <div 
                           key={color.id} 
                           onClick={() => selectColor(color.id)}
                           className={selectedColor === color.id ?  `cypcle cypcle-${color.name} selected` : `cypcle cypcle-${color.name}`}>
                        </div>
                     )) : null}
                  </div>
                  <button 
                     onClick={(e) => {    
                           addList(e);
                        }}
                     className="folder-modal__button">
                     Добавить
                  </button>
               </div>
            </div>
            <Route exact path="/"> 
               <div className="workspace__block">
                  {lists.length === 0 && <h1 className="no-tasks">Задачи отсутствуют</h1>}
                  {lists.map(list => (
                     <Workspace
                     titleMini={true}
                     onRemoveTask={onRemoveTask}
                     key={list.id} 
                     onAddTask={onAddTask} 
                     onEditTitle={onEditListTitle} 
                     list={list}
                     onEditTask={onEditTask}
                     onComplete={onCompleteTask}
                     ucFirst={ucFirst}
                     onImportantTask={onImportantTask}/>
                  ))}
                  
               </div>
            </Route>

            <Route path={`/lists/:id`}>    

               {activeItem && lists && (
                  <Workspace 
                     titleMini={false}
                     onAddTask={onAddTask} 
                     onRemoveTask={onRemoveTask}
                     onEditTitle={onEditListTitle} 
                     list={activeItem}
                     onEditTask={onEditTask}
                     onComplete={onCompleteTask}
                     ucFirst={ucFirst}
                     onImportantTask={onImportantTask}/>
               )}
            </Route>
         </>
         ) : null}
         <Route path="/privateOffice"><PrivateOffice onEditUser={onEditUser} setUser={setUser} setStatus={setStatus} user={user ? user : null} ucFirst={ucFirst}/></Route>

      </>
   )
}

export default  SidePanel;