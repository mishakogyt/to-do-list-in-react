import React, {useState} from 'react';
import axios from 'axios'
import {Link} from "react-router-dom";

import './folderItem.css';
import deleteImg from './img/delete.svg';

const FolderItem = ({items, onRemove, onClickItem, activeItem}) =>{
    const [selectItem, setSelectItem] = useState(null);
    const [hoverItem, setHovereItem] = useState(null);
    
    const onRemoveItem = item => {
        if(window.confirm('Удалить?')) {
            axios.delete('http://localhost:3001/lists/' + item.id).then(() => {
                onRemove(item.id);
            });
        }
    }
    
    return (
        <div className="folder-items" >
        {items.map((item) => (
            <Link to={`/lists/${item.id}`} key={item.id}>
                <div 
                    key={item.id} 
                    onClick={() => {
                        setSelectItem(item.id);
                        onClickItem(item);
                    }}
                    onMouseOver={() => setHovereItem(item.id)} 
                    onMouseOut={() => setHovereItem(null)}
                    className={activeItem && selectItem === item.id ? 'folder-item flex active' : 'folder-item flex'}>
                    <div className="folder-item__title flex">
                        <div className={`folder-item__cyrcle cypcle-${item.color ? item.color.name : null}`}></div>
                        <p>{item.name}<span>{items[0].tasks && item.tasks.length > 0  && `${item.tasks.length}`}</span></p>
                    </div>
                    <div className="folder-item__delete">
                        {/* eslint-disable-next-line */}
                        <img onClick={() => onRemoveItem(item)} src={hoverItem === item.id || activeItem && selectItem === item.id ? deleteImg : null} alt=""></img>
                    </div>
                </div>  
            </Link>          
        ))}
        </div>
    )
}

export default FolderItem;