import React, {useState} from 'react';
import axios from 'axios';
import {useHistory} from "react-router-dom";

import './privateOffice.css';

function PrivateOffice({user, onEditUser, setUser, setStatus, ucFirst}) {
   const [updateStatus, setUpdateStatus] = useState(false);
   const [login, setLogin] = useState("");
   const [password, setPassword] = useState("");
   const [name, setName] = useState("");
   const [email, setEmail] = useState("");
   let history = useHistory();
   
   const editUser = () => {
      axios.patch(`http://localhost:3001/users/${user.id}`, {
         "login": login ? login : user.login,
         "password": password ? password : user.password,
         "name": name ? name : user.name,
         "email": email ? email : user.email,
      })
      .then(( ) => {
         onEditUser();
         setUpdateStatus(true);
         setTimeout(() => {
            setUpdateStatus(false)
         }, 3000)
         })
      .catch(e => {
      alert('Ошибка');
      })
   }

   const deleteUser = async () => {
      await axios.get(`http://localhost:3001/users/${user.id}`)
      .then(({ data }) => {
         const userData = data
         console.log(data)
         axios.get(`http://localhost:3001/lists`)
         .then(({ data }) => {
            data.map(list => {
               if(userData && list.userId === userData.id){
                  axios.get(`http://localhost:3001/lists/${list.id}`)
                  .then(({ data }) => {
                     const userListData = data

                     axios.get(`http://localhost:3001/tasks`)
                     .then(({ data }) => {
                        data.map(task => {
                           if(userListData && task.listId === userListData.id){
                              axios.delete(`http://localhost:3001/tasks/${task.id}`) 
                           } 
                        }) 
                     })
                  })
               }

            })
            
         })

      })
      .finally(() => {
         axios.delete(`http://localhost:3001/users/${user.id}`)
         .catch(e => {
            alert(`Ошибка удаления пользователя ${user.name}`);
         })
         setUser(null);
         setStatus(false);
         history.push(`/`);
      })
   }

   return (
         <div className="private-office">
            <h3>Личный кабинет</h3>
         <div className="wrapp">
            <input 
               onChange={(e) => {                  
                  setName(ucFirst(e.target.value));
               }} 
               placeholder={user ? user.name : ""} 
               // value={name}
               >
            </input>
         </div>
         <div className="wrapp">
            <input 
               onChange={(e) => {                  
                  setLogin(e.target.value);
               }} 
               placeholder={user ? user.login : ""} 
               // value={user ? user.login : ""} 
               >   
            </input>
         </div>
         <div className="wrapp">
            <input 
               onChange={(e) => {                  
                  setPassword(e.target.value);
               }} 
               placeholder={user ? user.password : ""} 
               // value={user ? user.password : ""}
               >  
            </input>
         </div>
         <div className="wrapp">
            <input 
               onChange={(e) => {                  
                  setEmail(e.target.value);
                  console.log(login);
               }} 
               placeholder={user ? user.email : ""} 
               // value={user ? user.email : ""}
               >
            </input>
         </div>
         <div className="buttons">
            <button
               onClick={editUser}>
               Поменять
            </button>
            {/* <button
               onClick={deleteUser}>
               Удалить акаунт
            </button> */}
         </div>
            {updateStatus ? <p className="successful">Данные успешно сохранились</p> : null}
         </div>
   )
}

export default PrivateOffice;