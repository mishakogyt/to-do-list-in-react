import React, {useState} from 'react';
import axios from 'axios'

import './itemList.css';
import noCheckedSvg from './img/noChecked.svg';
import checkedSvg from './img/checked.svg';
import hoverCheckedSvg from './img/hoverChecked.svg';



function ItemList({tasks, list, onRemove, onEditTask, onComplete, ucFirst, onImportantTask}) {
    const [hoverCheсked, setHoverChecked] = useState(null);
    const [hoverItem, setHoverItem] = useState(null);

    const onEdit = (task, list) => {
        const newText = ucFirst(window.prompt('Название списка', task.text));
        if(newText) {
            onEditTask(task.id, list.id, newText);
           axios.patch('http://localhost:3001/tasks/' + task.id, {
            text: newText
           }).catch(() => {
              alert('Не удалось обновить название списка =(')
           });
        }
    } 

    const onImportant = (listId, taskId) => {
        onImportantTask(listId, taskId, list);
    };

     const onChangeCheckbox = (id, list) => {
        onComplete(list.id, id, list);
    };

    return (
        <>
            {tasks && !tasks.length && <h2>Добавь задачу!</h2>}
            {tasks && tasks.map((task, index) => (
                <div 
                    className="item-list" 
                    key={index} 
                    onMouseOver={() => {
                        setHoverItem(task.id);
                    }} 
                    onMouseOut={() => {
                        setHoverItem(null);
                    }}>
                    <img 
                        onMouseOver={() => {
                            setHoverChecked(task.id);
                        }} 
                        onMouseOut={() => {
                            setHoverChecked(null);
                        }}
                        src={(hoverCheсked === task.id && !task.completed ? hoverCheckedSvg : noCheckedSvg && task.completed ? checkedSvg : noCheckedSvg)} 
                        onClick={() => {
                            onChangeCheckbox(task.id, list);

                        }}
                        className="checked"
                        alt="checked">    
                    </img>
                    <p>{task.text}</p>

                    <svg onClick={() => {
                            onImportant(list.id, task.id);
                        }} 
                        className="important-svg"
                        xmlns="http://www.w3.org/2000/svg"   
                        viewBox="0 0 24 24" fill="#000" 
                        stroke="currentColor" 
                        strokeWidth="2" 
                        strokeLinecap="round" 
                        strokeLinejoin="round">
                        <polygon points={task.important === true || hoverItem === task.id ? "12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2" : null}>
                        </polygon>
                    </svg>

                    <svg onClick={() => onEdit(task, list)} className="edit-svg" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d={hoverItem === task.id ? "M0 12.0504V14.5834C0 14.8167 0.183308 15 0.41661 15H2.9496C3.05792 15 3.16624 14.9583 3.24123 14.875L12.34 5.78458L9.21542 2.66001L0.124983 11.7504C0.0416611 11.8338 0 11.9337 0 12.0504ZM14.7563 3.36825C14.8336 3.29116 14.8949 3.1996 14.9367 3.0988C14.9785 2.99801 15 2.88995 15 2.78083C15 2.6717 14.9785 2.56365 14.9367 2.46285C14.8949 2.36205 14.8336 2.27049 14.7563 2.19341L12.8066 0.24367C12.7295 0.166428 12.638 0.105146 12.5372 0.0633343C12.4364 0.021522 12.3283 0 12.2192 0C12.1101 0 12.002 0.021522 11.9012 0.0633343C11.8004 0.105146 11.7088 0.166428 11.6318 0.24367L10.107 1.76846L13.2315 4.89304L14.7563 3.36825V3.36825Z" : null}/>
                    </svg>

                    <svg 
                        onClick={() => onRemove(list.id, task.id)} 
                        id="Layer_1" enableBackground="new 0 0 512 512" height="15" viewBox="0 0 512 512" width="15" xmlns="http://www.w3.org/2000/svg">
                    { hoverItem === task.id ? <g>
                            <path d="m424 64h-88v-16c0-26.467-21.533-48-48-48h-64c-26.467 0-48 21.533-48 48v16h-88c-22.056 0-40 17.944-40 40v56c0 
                            8.836 7.164 16 16 16h8.744l13.823 290.283c1.221 25.636 22.281 45.717 47.945 45.717h242.976c25.665 0 46.725-20.081 
                            47.945-45.717l13.823-290.283h8.744c8.836 0 16-7.164 16-16v-56c0-22.056-17.944-40-40-40zm-216-16c0-8.822 7.178-16 
                            16-16h64c8.822 0 16 7.178 16 16v16h-96zm-128 56c0-4.411 3.589-8 8-8h336c4.411 0 8 3.589 8 8v40c-4.931 0-331.567 
                            0-352 0zm313.469 360.761c-.407 8.545-7.427 15.239-15.981 15.239h-242.976c-8.555 0-15.575-6.694-15.981-15.239l-13.751-288.761h302.44z" />
                            <path d="m256 448c8.836 0 16-7.164 16-16v-208c0-8.836-7.164-16-16-16s-16 7.164-16 16v208c0 8.836 7.163 16 16 16z" />
                            <path d="m336 448c8.836 0 16-7.164 16-16v-208c0-8.836-7.164-16-16-16s-16 7.164-16 16v208c0 8.836 7.163 16 16 16z" />
                            <path d="m176 448c8.836 0 16-7.164 16-16v-208c0-8.836-7.164-16-16-16s-16 7.164-16 16v208c0 8.836 7.163 16 16 16z" /> 
                        </g> : null}
                    </svg>
                </div>
            ))}
        </>
    )
}

export default ItemList ;